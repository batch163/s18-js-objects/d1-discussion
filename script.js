console.log('JS Objects');

let nums = [1,2,3,4,5];
nums[10] = 11
console.log(nums[6])
console.log(nums[7])

console.log(nums)


/*
	Object 
		- collection of data/ containers for data values.
		- These data values can be of different data types.
*/

let person = {
	//property: value
	firstName: "Mauro",
	lastName: "Muñoz",
	age: 16,
	address: {
		city: "Manila",
		country: "Philippines"
	},
	isMale: true,
	email: [
		"mm@mail.com", 
		"mauro@gmail.com", 
		"muñoz@yahoo.com"
	],
	spouse: null,
	fullName: function(){
		// console.log("firstName " + "lastName")
		// return this.firstName + " " + this.lastName
		return `${this.firstName} ${this.lastName}`
	}
}


// console.log(person);

// Q: how do we access property of an object?
	//dot notation
	//bracket notation

		//object.propertyName
		//object["propertyName"]

console.log(person.firstName);
console.log(person["lastName"]);

console.log(person.address.city);
console.log(person["address"]["country"]);
console.log(person["address"].city);

console.log(person.email[2]);

console.log(person.fullName());

person.job = "web developer";
console.log(person.job);

// delete person.job


let name = "Name";

console.log(person["first" + name]);
console.log(person["last" + name]);


person.friends = ["Teejae", "Arvin", "Alvin", "Alan"];

console.log("Mini Activity:");

console.log(
`
My name is ${person.fullName()}.
You can call me ${person.firstName}. 
I am ${person.age} years old and I live in ${person.address.city}, ${person.address.country}.
I work as a ${person.job} at Zuitt Coding Bootcamp.
My email addresses are ${person.email[0]}, ${person.email[1]} and ${person.email[2]}. 
Sirs ${person.friends[0]}, ${person.friends[1]}, ${person.friends[2]}, and ${person.friends[3]} are ${person.friends.length} of my friends at Zuitt.
`
)



/* Mini Activity
 
Given a set of objects:

- Compute for the average grade of each student object.
- Add the computed average of the student as a value to a new property called average to each element in the studentGrades array.
-Log the modified object array (includes the average property) to the console.

Stretch Goals:
-Round off the average into a single decimal number.

Note: Value for average property cannot be a string.

*/


const studentGrades = [
    { studentId: 1, Q1: 89.3, Q2: 91.2, Q3: 93.3, Q4: 89.8 },
    { studentId: 2, Q1: 69.2, Q2: 71.3, Q3: 76.5, Q4: 81.9 },
    { studentId: 3, Q1: 95.7, Q2: 91.4, Q3: 90.7, Q4: 85.6 },
    { studentId: 4, Q1: 86.9, Q2: 74.5, Q3: 83.3, Q4: 86.1 },
    { studentId: 5, Q1: 70.9, Q2: 73.8, Q3: 80.2, Q4: 81.8 }
];

// Solution 1: Manual Computation
/*
let total1 = studentGrades[0].Q1 + studentGrades[0].Q2 + studentGrades[0].Q3 + studentGrades[0].Q4

let ave1 = total1/4

studentGrades[0].average = ave1;

*/

// Solution 2: Using a Loop
/*
for(let i = 0; i < studentGrades.length; i++){
	// console.log(studentGrades[i].Q1)

	let total = studentGrades[i].Q1 + studentGrades[i].Q2 + studentGrades[i].Q3 + studentGrades[i].Q4

	let ave = total/4

	studentGrades[i].average = parseFloat(ave.toFixed(1))
}

console.log(studentGrades)

*/

// Solution 3: Using an array iterator forEach() method
/*
studentGrades.forEach(function(element){
	let total = element.Q1 + element.Q2 + element.Q3 + element.Q4
	let ave = total/4

	element.average = parseFloat(ave.toFixed(1))

})

console.log(studentGrades)
*/
/*
let digits = [5, 11, 30, 118, 634];

let total = digits.reduce(function(a, b){
	return a + b
})

console.log(total/digits.length)
*/


let hero = {
	name: "Angela",
	level: 1,
	role: "support",
	health: 100,
	items: {
		cd: "fleeting time",
		power: "genius wand"
	},
	attack: 50
}


// _____________________________________________
/*
let hero = {
	name: "Angela",
	level: 1,
	role: "support",
	health: 100,
	items: {
		cd: "fleeting time",
		power: "genius wand"
	},
	attack: 50
}
*/
// this is not an effecient way if you will be creating many objects of the same properties


// Object Constructor
	// is a template/blueprint

function Pokemon(name, lvl, health){
	this.name = name;
	this.level = lvl;
	this.health = health * 2
	this.attack = lvl
	this.intro = function(opponent){
		//const {name} = opponent	//object destructuring

		console.log(`Hi I'm ${this.name} and this is ${opponent.name}`)
	}
}


let squirtle = new Pokemon("Squirtle", 2, 50);
let bulbasaur = new Pokemon("Bulbasaur", 5, 100);

squirtle.intro(bulbasaur)

